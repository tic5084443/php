<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Triángulo Isósceles en PHP</title>
</head>
<body>

<pre><?php echo dibujarTrianguloIsosceles(5); ?></pre>
<pre><?php echo dibujarRombo(5); ?></pre>

<?php
function dibujarTrianguloIsosceles($filas) {
    $resultado = '';
    for ($i = 1; $i <= $filas; $i++) {
        $espacios = str_repeat(' ', $filas - $i);
        $asteriscos = str_repeat('*', 2 * $i - 1);
        $resultado .= $espacios . $asteriscos . "<br>";
    }
    return $resultado;
}

function dibujarRombo($filas) {
    $resultado = '';

    // Parte superior del rombo
    for ($i = 1; $i <= $filas; $i++) {
        $espacios = str_repeat(' ', $filas - $i);
        $asteriscos = str_repeat('*', 2 * $i - 1);
        $resultado .= $espacios . $asteriscos . "<br>";
    }

    // Parte inferior del rombo
    for ($i = $filas - 1; $i >= 1; $i--) {
        $espacios = str_repeat(' ', $filas - $i);
        $asteriscos = str_repeat('*', 2 * $i - 1);
        $resultado .= $espacios . $asteriscos . "<br>";
    }

    return $resultado;
}
?>

</body>
</html>
