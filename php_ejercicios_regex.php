<?php

function escapar_caracteres_especiales($entrada) {
    $caracteres_especiales = ['\\', '^', '$', '.', '[', '|', '(', ')', '?', '*', '+', '{', '}'];
    $caracteres_escapados = array_map(function ($char) {
        return '\\' . $char;
    }, $caracteres_especiales);

    return str_replace($caracteres_especiales, $caracteres_escapados, $entrada);
}

$email_regex = '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/';
$curp_regex = '/^[A-Z]{4}[0-9]{6}[A-Z]{6}[0-9]{2}$/';
$palabra_larga_regex = '/^[A-Za-z]{51,}$/';
$decimal_regex = '/^\d+(\.\d+)?$/';
?>
