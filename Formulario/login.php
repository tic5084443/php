<?php
session_start();

if (isset($_SESSION['user'])) {
    header("Location: info.php");
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $numero_cuenta = $_POST['numero_cuenta'];
    $contrasena = $_POST['contrasena'];

    $admin_numero_cuenta = 1;
    $admin_contrasena = 'adminpass123';

    if ($numero_cuenta == $admin_numero_cuenta && $contrasena == $admin_contrasena) {
        $_SESSION['user'] = [
            'numero_cuenta' => $admin_numero_cuenta,
            'nombre' => 'Admin',
            'fecha_nacimiento' => 'Fecha de Nacimiento Admin',
        ];
        header("Location: info.php");
        exit();
    } else {

        foreach ($_SESSION['alumnos'] as $alumno) {
            if ($numero_cuenta == $alumno['numero_cuenta'] && $contrasena == $alumno['contrasena']) {

                $_SESSION['user'] = [
                    'numero_cuenta' => $alumno['numero_cuenta'],
                    'nombre' => $alumno['nombre'],
                    'fecha_nacimiento' => $alumno['fecha_nacimiento'],
                ];
                header("Location: info.php");
                exit();
            }
        }

        $error = "Credenciales incorrectas";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="styles.css">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <form method="post" action="">
            <label for="numero_cuenta">Número de cuenta:</label>
            <input type="text" name="numero_cuenta" required>

            <label for="contrasena">Contraseña:</label>
            <input type="password" name="contrasena" required>

            <button type="submit">Iniciar Sesión</button>
        </form>

        <?php if (isset($error)) : ?>
            <p class="error"><?php echo $error; ?></p>
        <?php endif; ?>
    </div>
</body>
</html>
