<?php
session_start();

if (!isset($_SESSION['user'])) {
    header("Location: login.php");
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $_SESSION['alumnos'][] = [
        'numero_cuenta' => $_POST['num_cta'],
        'nombre' => $_POST['nombre'],
        'primer_apellido' => $_POST['primer_apellido'],
        'segundo_apellido' => $_POST['segundo_apellido'],
        'genero' => $_POST['genero'],
        'fecha_nacimiento' => $_POST['fec_nac'],
        'contrasena' => $_POST['contrasena'],
    ];
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="styles.css">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario de Alumnos</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <h1>Formulario de Alumnos</h1>
        <form method="post" action="">
            <label for="num_cta">Número de cuenta:</label>
            <input type="text" name="num_cta" required>

            <label for="nombre">Nombre:</label>
            <input type="text" name="nombre" required>

            <label for="primer_apellido">Primer Apellido:</label>
            <input type="text" name="primer_apellido" required>

            <label for="segundo_apellido">Segundo Apellido:</label>
            <input type="text" name="segundo_apellido" required>

            <label for="genero">Género:</label>
            <select name="genero" required>
                <option value="M">Hombre</option>
                <option value="F">Mujer</option>
                <option value="O">Otro</option>
            </select>

            <label for="fec_nac">Fecha de Nacimiento:</label>
            <input type="text" name="fec_nac" placeholder="dd/mm/aaaa" required>

            <label for="contrasena">Contraseña:</label>
            <input type="password" name="contrasena" required>

            <button type="submit">Guardar</button>
        </form>

        <a href="info.php">Ir a Información</a>
       
        <a href="logout.php">Cerrar Sesión</a>
    </div>
</body>
</html>
