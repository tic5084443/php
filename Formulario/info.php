<?php
session_start();

if (!isset($_SESSION['user'])) {
    header("Location: login.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="styles.css">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Información del Usuario</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <h1>Bienvenido, <?php echo $_SESSION['user']['nombre']; ?>!</h1>
        <p>Fecha de Nacimiento: <?php echo $_SESSION['user']['fecha_nacimiento']; ?></p>
        <p>Número de Cuenta: <?php echo $_SESSION['user']['numero_cuenta']; ?></p>

        <h2>Usuarios Registrados</h2>
        <?php if (isset($_SESSION['alumnos'])) : ?>
            <ul>
                <?php foreach ($_SESSION['alumnos'] as $alumno) : ?>
                    <li>
                        Número de Cuenta: <?php echo $alumno['numero_cuenta']; ?>,
                        Nombre: <?php echo $alumno['nombre']; ?>,
                        Fecha de Nacimiento: <?php echo $alumno['fecha_nacimiento']; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

        <a href="formulario.php">Ir a Formulario</a>
        <a href="logout.php">Cerrar Sesión</a>
    </div>
</body>
</html>

